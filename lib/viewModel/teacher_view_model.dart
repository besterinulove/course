import 'package:courses/model/user_model.dart';
import 'package:courses/service/course_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final teacherViewModel = NotifierProvider.autoDispose<TeacherViewModel, TeacherViewModelState>(TeacherViewModel.new);

class TeacherViewModelState {
  final List<User> teachers;

  TeacherViewModelState({this.teachers = const []});

  TeacherViewModelState copyWith({
    List<User>? teachers,
  }) {
    return TeacherViewModelState(
      teachers: teachers ?? this.teachers,
    );
  }
}

class TeacherViewModel extends AutoDisposeNotifier<TeacherViewModelState> {
  final _service = FakeService();

  Future getTeacherList() {
    return _service.getTeacherList().then((value) => state = state.copyWith(teachers: value));
  }

  @override
  TeacherViewModelState build() {
    return TeacherViewModelState();
  }
}
