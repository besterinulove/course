import 'dart:convert';
import 'dart:ffi';

import 'package:courses/model/course_model.dart';
import 'package:courses/model/user_model.dart';
import 'package:courses/model/users_model.dart';
import 'package:dio/dio.dart';
import 'package:collection/collection.dart';

class Service {
  Future<List<Course>?> getCourseList() {
    return Dio().get<List<Course>>('/api/courses').then((value) {
      return value.data;
    });
  }

  Future<List<User>?> getTeacherList() {
    return Dio().get<List<User>>('/api/users', queryParameters: {'isTeacher': true}).then((value) {
      return value.data;
    });
  }

  Future<List<Course>?> getTeacherCourses(int teacherId) {
    return Dio().get<List<Course>>('/api/courses/$teacherId').then((value) {
      return value.data;
    });
  }

  Future<User?> createTeacher(String userName,String password,String name) {
    return Dio().post<User>('/api/users', data: {'userName':userName,'password':password,'name': name, 'isTeacher': true}).then((value) =>value.data);
  }

  Future<Course?> createCourse(String name, String schedule) {
   return Dio().post<Course>('/api/courses', data: {'name': name, 'schedule': schedule}).then((value) =>value.data);
  }

  Future<Course?> updateCourse(Course course) {
    return Dio().put<Course>('/api/courses/${course.id}', data: {'name': course.name, 'schedule': course.schedule}).then((value) =>value.data);
  }

  Future<String> deleteCourse(int courseId) {
   return Dio().delete('/api/courses/$courseId').then((value) =>'success');
  }
}

class FakeService implements Service {
  @override
  Future<Course?> createCourse(String name, String schedule) {
    return Future.value(Course(name: name,schedule: schedule));
  }

  @override
  Future<User?> createTeacher(String userName,String password,String name) {
    return Future.value(User(userName: userName,password: password,name: name,isTeacher: true,avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR78YQNnfwQX6q8i9IUmbyL9YA9PxlloC0sC_DORVf1kQ&s'));
  }

  @override
  Future<String> deleteCourse(int courseId) {
    return Future.value('success');
  }

  @override
  Future<List<Course>?> getCourseList() {
    return Future.value(UsersModel.fromJson(jsonDecode(UsersModel.fakeData)).users?.map((e) => e.courses).expand((element) => element??<Course>[]).toList());
  }

  @override
  Future<List<Course>?> getTeacherCourses(int teacherId) {
    return Future.value(UsersModel.fromJson(jsonDecode(UsersModel.fakeData)).users?.firstWhereOrNull((element) => element.id==teacherId)?.courses);
  }

  @override
  Future<List<User>?> getTeacherList() {
    return Future.value(UsersModel.fromJson(jsonDecode(UsersModel.fakeData)).users);
  }

  @override
  Future<Course> updateCourse(Course course) {
    return Future.value(course);
  }
}
