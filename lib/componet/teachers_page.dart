import 'package:courses/componet/teacher_item.dart';
import 'package:courses/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../viewModel/teacher_view_model.dart';

final currentTeacher = Provider<User>((ref) => throw UnimplementedError());

class TeacherPage extends ConsumerStatefulWidget {
  const TeacherPage({super.key});

  @override
  ConsumerState<TeacherPage> createState() => _TeacherPageState();
}

class _TeacherPageState extends ConsumerState<TeacherPage> {
  @override
  void initState() {
    super.initState();
    ref.read(teacherViewModel.notifier).getTeacherList();
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(teacherViewModel);
    return Scaffold(
      appBar: AppBar(
        title: const Text('講師清單'),
        centerTitle: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(16),
        itemBuilder: (context, index) {
          return ProviderScope(
              overrides: [currentTeacher.overrideWithValue(state.teachers[index])], child: const TeacherItem());
        },
        itemCount: state.teachers.length,
        separatorBuilder: (context, index) {
          return const SizedBox(
            height: 16,
          );
        },
      ),
    );
  }
}
