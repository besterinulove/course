import 'package:courses/model/course_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final currentCourse = Provider<Course>((ref) => throw UnimplementedError());

class CoursePage extends ConsumerWidget {
  const CoursePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final course = ref.read(currentCourse);
    return Scaffold(
      appBar: AppBar(
        title: Text(course.name ?? ''),
        centerTitle: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: Column(
        children: [
          Text(course.id.toString()),
          Text(course.schedule??'')
        ],
      ),
    );
  }
}
