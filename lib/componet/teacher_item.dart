import 'package:courses/componet/course_page.dart';
import 'package:courses/componet/teachers_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TeacherItem extends ConsumerStatefulWidget {
  const TeacherItem({super.key});

  @override
  ConsumerState<TeacherItem> createState() => _TeacherItemState();
}

class _TeacherItemState extends ConsumerState<TeacherItem> {
  final shape =
      const ContinuousRectangleBorder(side: BorderSide(width: 1), borderRadius: BorderRadius.all(Radius.circular(10)));

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final teacher = ref.read(currentTeacher);
    return ExpansionTile(
      collapsedShape: shape,
      shape: shape,
      leading: ClipOval(child: Image.network(teacher.avatar ?? '')),
      title: Text(
        teacher.description ?? '',
        style: const TextStyle(color: Colors.black26),
      ),
      subtitle: Text(teacher.name ?? ''),
      trailing: const Icon(Icons.add),
      children: (teacher.courses?.map((course) {
            Widget widget = Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: ListTile(
                leading: const Icon(Icons.date_range),
                title: Text(course.name ?? ''),
                subtitle: Text(course.schedule ?? ''),
                trailing: const Icon(Icons.chevron_right),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProviderScope(
                            overrides: [currentCourse.overrideWithValue(course)], child: const CoursePage()),
                      ));
                },
              ),
            );
            return widget;
          }).toList()
            ?..insert(
                0,
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Divider(
                    color: Colors.grey,
                  ),
                ))) ??
          [],
    );
  }
}
