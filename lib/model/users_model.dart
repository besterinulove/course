
import 'user_model.dart';

/// users : [{"id":1,"name":"Albert Flores","userName":"west001","password":"1234","description":"Demostator","isTeacher":true,"courses":[{"id":1,"teacher_id":1,"name":"基礎程式設計","schedule":"每週二,10:00-12:00"},{"id":2,"teacher_id":1,"name":"人工智慧總整與實作","schedule":"每週四,14:00-16:00"},{"id":3,"teacher_id":1,"name":"訊號與系統","schedule":"每週五,10:00-12:00"}]},{"id":2,"name":"Floyd Miles","userName":"west002","password":"1234","description":"Leturer","isTeacher":true,"courses":[{"id":4,"teacher_id":2,"name":"基礎程式設計","schedule":"每週二,10:00-12:00"},{"id":5,"teacher_id":2,"name":"人工智慧總整與實作","schedule":"每週四,14:00-16:00"},{"id":6,"teacher_id":2,"name":"訊號與系統","schedule":"每週五,10:00-12:00"}]},{"id":3,"name":"Savannah Nguyen","userName":"west003","password":"1234","description":"Senior Leturer","isTeacher":true,"courses":[{"id":7,"teacher_id":3,"name":"基礎程式設計","schedule":"每週二,10:00-12:00"},{"id":8,"teacher_id":3,"name":"人工智慧總整與實作","schedule":"每週四,14:00-16:00"},{"id":9,"teacher_id":3,"name":"訊號與系統","schedule":"每週五,10:00-12:00"}]},{"id":4,"name":"Jenny Wilson","userName":"west004","password":"1234","description":"Prodoctor","isTeacher":true,"courses":[{"id":10,"teacher_id":4,"name":"基礎程式設計","schedule":"每週二,10:00-12:00"},{"id":11,"teacher_id":4,"name":"人工智慧總整與實作","schedule":"每週四,14:00-16:00"},{"id":12,"teacher_id":4,"name":"訊號與系統","schedule":"每週五,10:00-12:00"}]},{"id":5,"name":"Floyd Miles","userName":"west005","password":"1234","description":"Prodoctor","isTeacher":true,"courses":[{"id":13,"teacher_id":5,"name":"基礎程式設計","schedule":"每週二,10:00-12:00"},{"id":14,"teacher_id":5,"name":"人工智慧總整與實作","schedule":"每週四,14:00-16:00"},{"id":15,"teacher_id":5,"name":"訊號與系統","schedule":"每週五,10:00-12:00"}]}]

class UsersModel {
  static const fakeData='''
  {
  "users":[
    {
      "id":1,
      "name":"Albert Flores",
      "userName":"west001",
      "password":"1234",
      "description":"Demostator",
      "isTeacher":true,
      "avatar":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR78YQNnfwQX6q8i9IUmbyL9YA9PxlloC0sC_DORVf1kQ&s",
      "courses":[
        {
          "id":1,
          "teacher_id":1,
          "name":"基礎程式設計",
          "schedule":"每週二,10:00-12:00"
        },
        {
          "id":2,
          "teacher_id":1,
          "name":"人工智慧總整與實作",
          "schedule":"每週四,14:00-16:00"
        },
        {
          "id":3,
          "teacher_id":1,
          "name":"訊號與系統",
          "schedule":"每週五,10:00-12:00"
        }
      ]
    },
    {
      "id":2,
      "name":"Floyd Miles",
      "userName":"west002",
      "password":"1234",
      "description":"Leturer",
      "isTeacher":true,
      "avatar":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0tpmn7XqEyJ-UZxdlHsEh3BE-xLDvolqhgEmgYB9WcQ&s",
      "courses":[
        {
          "id":4,
          "teacher_id":2,
          "name":"基礎程式設計",
          "schedule":"每週二,10:00-12:00"
        },
        {
          "id":5,
          "teacher_id":2,
          "name":"人工智慧總整與實作",
          "schedule":"每週四,14:00-16:00"
        },
        {
          "id":6,
          "teacher_id":2,
          "name":"訊號與系統",
          "schedule":"每週五,10:00-12:00"
        }
      ]
    },
    {
      "id":3,
      "name":"Savannah Nguyen",
      "userName":"west003",
      "password":"1234",
      "description":"Senior Leturer",
      "isTeacher":true,
      "avatar":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0yXAVP_1RKX_tKaGZ1xzLVsardRu0LpMr647BQUY6TQ&s",
      "courses":[
        {
          "id":7,
          "teacher_id":3,
          "name":"基礎程式設計",
          "schedule":"每週二,10:00-12:00"
        },
        {
          "id":8,
          "teacher_id":3,
          "name":"人工智慧總整與實作",
          "schedule":"每週四,14:00-16:00"
        },
        {
          "id":9,
          "teacher_id":3,
          "name":"訊號與系統",
          "schedule":"每週五,10:00-12:00"
        }
      ]
    },
    {
      "id":4,
      "name":"Jenny Wilson",
      "userName":"west004",
      "password":"1234",
      "description":"Prodoctor",
      "isTeacher":true,
      "avatar":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvCnCypnRLsZmJc1qKB18ac-vIWzUPIyZZ_ZLjtBtSsQ&s",
      "courses":[
        {
          "id":10,
          "teacher_id":4,
          "name":"基礎程式設計",
          "schedule":"每週二,10:00-12:00"
        },
        {
          "id":11,
          "teacher_id":4,
          "name":"人工智慧總整與實作",
          "schedule":"每週四,14:00-16:00"
        },
        {
          "id":12,
          "teacher_id":4,
          "name":"訊號與系統",
          "schedule":"每週五,10:00-12:00"
        }
      ]
    },
    {
      "id":5,
      "name":"Floyd Miles",
      "userName":"west005",
      "password":"1234",
      "description":"Prodoctor",
      "isTeacher":true,
      "avatar":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAR5YZb5TpSKh38PovotFJF8zjrXDkPA6O9MgTOhltZg&s",
      "courses":[
        {
          "id":13,
          "teacher_id":5,
          "name":"基礎程式設計",
          "schedule":"每週二,10:00-12:00"
        },
        {
          "id":14,
          "teacher_id":5,
          "name":"人工智慧總整與實作",
          "schedule":"每週四,14:00-16:00"
        },
        {
          "id":15,
          "teacher_id":5,
          "name":"訊號與系統",
          "schedule":"每週五,10:00-12:00"
        }
      ]
    }
  ]
}
  ''';

  UsersModel({
      List<User>? users,}){
    _users = users;
}

  UsersModel.fromJson(dynamic json) {
    if (json['users'] != null) {
      _users = [];
      json['users'].forEach((v) {
        _users?.add(User.fromJson(v));
      });
    }
  }
  List<User>? _users;
UsersModel copyWith({  List<User>? users,
}) => UsersModel(  users: users ?? _users,
);
  List<User>? get users => _users;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_users != null) {
      map['users'] = _users?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

