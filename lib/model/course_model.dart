
/// id : 1
/// teacher_id : 1
/// name : "基礎程式設計"
/// schedule : "每週二,10:00-12:00"

class Course {
  Course({
    int? id,
    int? teacherId,
    String? name,
    String? schedule,}){
    _id = id;
    _teacherId = teacherId;
    _name = name;
    _schedule = schedule;
  }

  Course.fromJson(dynamic json) {
    _id = json['id'];
    _teacherId = json['teacher_id'];
    _name = json['name'];
    _schedule = json['schedule'];
  }
  int? _id;
  int? _teacherId;
  String? _name;
  String? _schedule;
  Course copyWith({  int? id,
    int? teacherId,
    String? name,
    String? schedule,
  }) => Course(  id: id ?? _id,
    teacherId: teacherId ?? _teacherId,
    name: name ?? _name,
    schedule: schedule ?? _schedule,
  );
  int? get id => _id;
  int? get teacherId => _teacherId;
  String? get name => _name;
  String? get schedule => _schedule;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['teacher_id'] = _teacherId;
    map['name'] = _name;
    map['schedule'] = _schedule;
    return map;
  }

}