import 'package:courses/model/course_model.dart';
import 'package:courses/service/course_service.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final service = FakeService();
  test('createCourse', () {
    String name = '基礎程式設計';
    String schedule = '基礎程式設計';

    service.createCourse(name, schedule).then((value) {
      expect(value?.name, name);
      expect(value?.schedule, schedule);
    });
  });

  test('createTeacher', () {
    String userName = 'west666';
    String password = '11111111';
    String name = '威斯特';

    service.createTeacher(userName, password, name).then((value) {
      expect(value?.userName, userName);
      expect(value?.password, password);
      expect(value?.name, name);
    });
  });

  test('updateCourse', () {
    final course=Course(id: 1,teacherId: 1,name: '基礎程式設計',schedule: '每週二,10:00-12:00');

    service.updateCourse(course).then((value) {
      expect(value.name, course.name);
      expect(value.schedule, course.schedule);
      expect(value.id, course.id);
      expect(value.teacherId, course.teacherId);
    });
  });
}
